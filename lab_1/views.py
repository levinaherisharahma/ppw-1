from django.shortcuts import render
from datetime import datetime, date


# Enter your name here
mhs_name = 'Levina Herisha Rahma' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 2, 3) #TODO Implement this, format (Year, Month, Date)
npm = 1806191263 # TODO Implement this
# Create your views here.
tempat_kuliah = 'Information Systems 2018, University of Indonesia'
hobi = 'drawing, crafting and playing table tennis'
deskripsi = '''
            I'm very shy to new people and i don't talk much to them, but
            I can be a very silly person to my close friends. I'm also a
            bit of an airhead. I really, REALLY love cats but unfortunately
            my parents won't allow me to keep them.
            '''


def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
                'kuliah': tempat_kuliah, 'hobi': hobi, 'deskripsi': deskripsi}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
